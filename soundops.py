# Ejercicio 7
# Ejercicio 9

from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:
    duracion = min(s1.duration, s2.duration)
    buffer = [s1.buffer[i] + s2.buffer[i] for i in range(duracion)]

    result = Sound(duracion)
    result.buffer = buffer

    return result
